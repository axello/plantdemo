//------------------------------------------------------------------------------
//
// PlantDemo.swift4a
// Swift For Arduino
//
// Created by Axel Roest on 12/18/2019.
// Copyright © 2019-2022 Axel Roest. All rights reserved.
//
// NOTE: Modifications to the "Libraries:" comment line below will affect the build.
// Libraries:
//------------------------------------------------------------------------------

import AVR
typealias IntegerLiteralType = Pin

//------------------------------------------------------------------------------
// Hardware Setup
//------------------------------------------------------------------------------
let iLEDPin = 8
let debugPin = 5
let hasWhiteChip = false
let pixelCount: UInt16 = 1
let waterSensor = 0

let blinkMilliseconds: UInt16 = 10_000
let sleepMicroseconds: LargeMicroseconds = 1_000_000
var blinkWatchdogLed = false

var index: UInt8 = 0

// NB water setpoints. In my tests with the moisture sensor,
// wet earth is around 300 and dry earth is about 340
var lowSetPoint: UInt16 = 300
var highSetPoint: UInt16 = 340

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

// Insert code here to setup IO pins, define properties, add functions, etc.
func setup() {
    // baudrate needs to be 9600 for osx because of a bug in SwiftSerial (?)
    SetupSerial(baudRate:9600, cpuClockRatekHz: 16_000)

    pinMode(pin: iLEDPin, mode: OUTPUT)
    pinMode(pin: debugPin, mode: OUTPUT)
    iLEDFastSetup(pin: iLEDPin, pixelCount: pixelCount, hasWhite: hasWhiteChip, grbOrdered: true)

    for _ in 1...pixelCount {
        iLEDFastWritePixel(color: iLEDOff)
    }
    // Latch so color is displayed
    delay(microseconds: 6)

    setupWatchDogBlinky()
    print(message:"Moisture detecting started")
}

func white() {
    for i in 1...255 {
        let blue: UInt8 = UInt8(i) % 255
        let dimRed = iLEDFastMakeColor(red: blue, green: blue, blue: blue, white: 0)
        for _ in 1...pixelCount {
            iLEDFastWritePixel(color: dimRed)
        }
        delay(microseconds: 6)
    }
}


func watchdogBlinky() {
    blinkWatchdogLed = false
    digitalWrite(pin: debugPin, value: HIGH)

    let steps = 10
    let ms: UInt16 = 5

//    static var lastTime: UInt16 = 0
//    for i in 0...steps {
//        let dimRed = iLEDFastMakeColor(red: 0, green: i * UInt8(ms), blue: 0, white: 0)
//        for _ in 1...pixelCount {
//            iLEDFastWritePixel(color: dimRed)
//        }
//        delay(milliseconds: ms)
//    }
//    for i in 0...steps {
//        let dimRed = iLEDFastMakeColor(red: 0, green: (steps - i) * UInt8(ms), blue: 0, white: 0)
//        for _ in 1...pixelCount {
//            iLEDFastWritePixel(color: dimRed)
//        }
//        delay(milliseconds: ms)
//    }

//    lastTime = ticks()
}

func setupWatchDogBlinky() {
    // regular callback in interrupt
    executeAsync(after: blinkMilliseconds, repeats: true) {
        blinkWatchdogLed = true
        digitalWrite(pin: debugPin, value: LOW)    // for debugging
    }
}

func sendValue(_ value: UInt16) {
    print(message: "[", addNewline:false)
    print(unsignedInt: value, addNewline: false)
    print(message: "]")
}

func processHumidity(pin analogPin: Pin) -> Bool {
    let value: UInt16 = analogReadSync(pin: analogPin)
    sendValue(value)
    return value < lowSetPoint
}

func showAlarm(_ ledPin: Pin) {
    let steps = 10
    let ms: UInt16 = 5

    for i in 0...steps {
        let dimRed = iLEDFastMakeColor(red: i * UInt8(ms) * 4, green: 0, blue: 0, white: 0)
        for _ in 1...pixelCount {
            iLEDFastWritePixel(color: dimRed)
        }
        //delay(microseconds: 6)
        delay(milliseconds: ms)
    }
    for i in 0...steps {
        let dimRed = iLEDFastMakeColor(red: (steps - i) * UInt8(ms) * 4, green: 0, blue: 0, white: 0)
        for _ in 1...pixelCount {
            iLEDFastWritePixel(color: dimRed)
        }
        delay(milliseconds: ms)
    }
}

func hideAlarm(_ ledPin: Pin) {
    for _ in 1...pixelCount {
        iLEDFastWritePixel(color: iLEDOff)
    }
}

//func readCommand() {
//    var finished = false
//    var lowValue: UInt16  = 0
//    var highValue: UInt16 = 0
//
//    while !finished {
//        var byte = read()
//
//        while byte != 0x56 {        // 'S'        // wait until setpoint command is received
////        print(byte: byte)
//            byte = read()
//        }
////        // byte is after 'S'; read low value
////        while byte >= 0x30 && byte <= 0x39 {      // between 0-9
////            lowValue = lowValue * 10 + UInt16(byte)
////            byte = read()
////        }
////        // byte != cipher
////         while byte >= 0x30 && byte <= 0x39 {      // between 0-9
////            highValue = highValue * 10 + UInt16(byte)
////            byte = read()
////        }
//        finished = true
//    }
//    print(message:"\nlow,high: ")
//    print(unsignedInt: lowValue)
//    print(message:", ")
//    print(unsignedInt: highValue)
//
//}

//------------------------------------------------------------------------------
// Set up
//------------------------------------------------------------------------------
setup()

//------------------------------------------------------------------------------
// Main Loop
//------------------------------------------------------------------------------
var humidityAlarm: Bool = false
var alarming: Bool = false            // if we are currently showing the alarm
while(true) {

    if blinkWatchdogLed {
        watchdogBlinky()
        humidityAlarm = processHumidity(pin: waterSensor)
    }
    if humidityAlarm {
        showAlarm(iLEDPin)
        alarming = true
    } else {
        if alarming {
            hideAlarm(iLEDPin)
            alarming = false            // only turn off leds once, not continuously
        }
        // sleeping the cpu conflicts with the wavy blink, but is more practical in real-world situations
        // sleepCpu(microseconds: sleepMicroseconds)
    }

//  if available() {
//      readCommand()
//  }
}


//------------------------------------------------------------------------------
